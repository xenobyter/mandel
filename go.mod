module bitbucket.org/xenobyter/mandel

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/alecthomas/units v0.0.0-20190924025748-f65c72e2690d // indirect
	github.com/ivpusic/go-clicolor v0.0.0-20150828210804-23f0b77f328a // indirect
	github.com/ivpusic/golog v0.0.0-20170608213328-28640bee649f // indirect
	github.com/ivpusic/neo v0.3.0
	github.com/ivpusic/rerun v0.0.0-20170331080801-adc8acf1481b // indirect
	github.com/ivpusic/urlregex v0.0.0-20160128000749-4dd86e784405 // indirect
	github.com/lucasb-eyer/go-colorful v1.0.3
	github.com/shiena/ansicolor v0.0.0-20151119151921-a422bbe96644 // indirect
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
	gopkg.in/alecthomas/kingpin.v2 v2.2.6 // indirect
	gopkg.in/fsnotify.v1 v1.4.7 // indirect
)
