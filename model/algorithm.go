package model

import (
	"fmt"
	"time"
)

// EscapeTime uses the following arguments to describe a Mandelbrot set:
// x1, x2, y1, y2, limit, maxIterations. The remaining arguments specify the size of the generated set.
// It returns the set as slice of slices.
func EscapeTime(x1, x2, y1, y2, limit float64, maxIterations, width, height uint) [][]uint {
	var mandel [][]uint
	var imgX, imgY uint
	// The Mandelbrot set is the set of complex numbers c for which the function f{c}(z)=z^{2}+c} does not
	// diverge when iterated from z=0
	fmt.Println("Starting escape time algorithm ...")
	t := time.Now()
	for imgY = 0; imgY < height; imgY++ {
		fmt.Printf("\rLine %d", imgY)
		var line []uint
		for imgX = 0; imgX < width; imgX++ {
			var i uint = 0
			x0 := scale(imgX, width, x1, x2)
			y0 := scale(imgY, height, y1, y2)
			x := 0.0
			y := 0.0
			x2 := 0.0
			y2 := 0.0
			for i < maxIterations && x2+y2 <= limit {
				// xTemp := x*x - y*y + x0
				// y = 2*x*y + y0
				// x = xTemp
				y = 2*x*y+y0
				x = x2 - y2 + x0 
				x2 = x*x
				y2 = y*y
				i++
			}
			line = append(line, i)
		}
		mandel = append(mandel, line)
	}
	fmt.Printf("\n... finished in %0.2vs\n", time.Now().Sub(t).Seconds())
	return mandel
}

// Scale the images pixels to coordinates of the mandelbrot set
func scale(pixel, screenSize uint, min, max float64) float64 {
	return float64(pixel)/float64(screenSize)*(max-min) + min
}
