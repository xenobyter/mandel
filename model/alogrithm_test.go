package model

import "testing"

func TestScale(t *testing.T) {
	got := scale(10, 100, 0.0, 10.0)
	want := 1.0
	if got != want {
		t.Errorf("scale: got %f, want %f", got, want)
	}
	got = scale(50, 100, -1.0, 1.0)
	want = 0.0
	if got != want {
		t.Errorf("scale: got %f, want %f", got, want)
	}
}

func TestEscapeTime(t *testing.T) {
	got := EscapeTime(-2.5, 1.0, -1.5, 1.5, 2.0, 10, 10, 10)
	want := []uint{1, 1, 1, 1, 3, 5, 10, 10, 10, 2}
	if !Equal(got[3],want) {
		t.Errorf("Line 3: got %v, want %v", got[3], want)
	}
}

// Equal tells whether a and b contain the same elements.
// A nil argument is equivalent to an empty slice.
func Equal(a, b []uint) bool {
    if len(a) != len(b) {
        return false
    }
    for i, v := range a {
        if v != b[i] {
            return false
        }
    }
    return true
}