window.onload = function(){
    document.getElementById("mandel").addEventListener("click", zoom)
}
function zoom(event) {
    const img = document.getElementById("mandel")
    const imgRect = img.getBoundingClientRect();

    //get old width and height from coordinates
    const oldWidth = +document.getElementById("x2").value - +document.getElementById("x1").value
    const oldHeight = +document.getElementById("y2").value - +document.getElementById("y1").value

    if (!(oldWidth === NaN && oldHeight === NaN)) {
        //calculate where the click was
        const xClick = ((event.clientX - imgRect.x) / img.width) * oldWidth + +document.getElementById("x1").value
        const yClick = (event.clientY - imgRect.y) / img.height * oldHeight + +document.getElementById("y1").value
        //calculate new width and height
        const shrink = 0.7

        //new values 
        document.getElementById("x1").value = xClick - 0.5 * shrink * oldWidth
        document.getElementById("x2").value = xClick + 0.5 * shrink * oldWidth
        document.getElementById("y1").value = yClick - 0.5 * shrink * oldHeight
        document.getElementById("y2").value = yClick + 0.5 * shrink * oldHeight

        //click the button
        document.getElementById("submit").click()
    }
}

