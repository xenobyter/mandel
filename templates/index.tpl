{{define "index"}}
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta
      http-equiv="Cache-Control"
      content="no-cache, no-store, must-revalidate"
    />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <title>Mandelbrot Generator</title>
    <link rel="stylesheet" href="/public/style.css" />
    <script src="/public/index.js"></script>
  </head>
  <body>
    <h1>Mandelbrot Generator</h1>
    <div class="content">
      <img class="mandel" id="mandel" src="/public/mandel.png" alt="Mandelbrot image" />
      <form action="/mandel" method="POST">
        <h2>Koordinaten</h2>
        <div class="axis-label">X-Achse:</div>
        <div class="inputs">
          <input type="text" name="x1" id="x1" value="{{.X1}}" />-
          <input type="text" name="x2" id="x2" value="{{.X2}}" />
        </div>
        <div class="axis-label">Y-Achse:</div>
        <div class="inputs">
          <input type="text" name="y1" id="y1" value="{{.Y1}}" />-
          <input type="text" name="y2" id="y2" value="{{.Y2}}" />
        </div>
        <h2>Iterationen</h2>
        <div class="inputs">
          <input type="text" name="itt" id="itt" value="{{.MaxIterations}}" />
        </div>
        <h2>Auflösung</h2>
        <div class="inputs">
          <input type="text" name="width" id="width" value="{{.Width}}" />
          x
          <input type="text" name="height" id="height" value="{{.Height}}" />
        </div>
        <div class="inputs">
          <input type="submit" id="submit" value="Berechnen" />
        </div>
      </form>
    </div>
  </body>
</html>
{{end}}