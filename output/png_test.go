package output

import (
	"os"
	"testing"
)

func TestImagePNG(t *testing.T) {
	//generate test-set
	testFileName := "test.png"
	var testSet [][]uint
	var x,y, width, height uint
	width = 100
	height = 100
	for x=0; x<width; x++ {
		var line []uint
		for y=0; y<height; y++ {
			line = append(line, y*3)
		}
		testSet = append(testSet, line)
	}
	ImagePNG(testSet,testFileName, width, height, 100)
	_, err := os.Stat(testFileName)
	if err != nil {
		t.Errorf("Testfile not generated: %v", err)
	}
	os.Remove(testFileName)
}