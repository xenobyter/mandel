package output

import (
	"image/color"
	"github.com/lucasb-eyer/go-colorful"
)

// Color takes the pixels iteration count and and returns a RGBA-color
func Color(iteration, max uint) color.RGBA {
	value := 0.7
	if iteration == max {
		value = 0
	}
	c := colorful.Hsv(float64((iteration*3+240)%360), 1, value)
	return color.RGBA{uint8(c.R*255), uint8(c.G*255), uint8(c.B*255), 255}
}
