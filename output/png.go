package output

import (
	"os"
	"image"
	"image/png"
)

// ImagePNG takes a Mandelbrot set as slice od slices and writes the colored png with the specified width and height.
func ImagePNG(mandel [][]uint, filename string, width, height, max uint) {
	// Create a blank image
	img := image.NewRGBA(image.Rect(0, 0, int(width), int(height)))

	// Color each pixel
	var x,y uint
	for x= 1;x<width;x++ {
		for y=1;y<height;y++ {
			img.Set(int(x),int(y), Color(mandel[y][x], max))
		}
	}

	// Save to png-File
	f, _ := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE, 0600)
	defer f.Close()
	png.Encode(f, img)
}