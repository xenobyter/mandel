package main

import (
	"flag"
	"fmt"
	"strconv"

	"bitbucket.org/xenobyter/mandel/model"
	"bitbucket.org/xenobyter/mandel/output"
	"github.com/ivpusic/neo"
	// "bitbucket.org/xenobyter/mandel/model"
	// "bitbucket.org/xenobyter/mandel/output"
)

// Arguments represents all parameters to calculate a mandelbrot set
type Arguments struct {
	X1, X2, Y1, Y2 *float64
	limit          *float64
	MaxIterations  *uint
	Width, Height  *uint
	ui             *bool
	imgFile        string
}

func (arg Arguments) String() string {
	return fmt.Sprintf(
		"Mandelbrot properties:\nx-axis: %f ... %f\ny-axis: %f ... %f\nmax iterations: %d, limit %f\nOutput: %s, size %dx%d",
		*arg.X1, *arg.X2, *arg.Y1, *arg.Y2, *arg.MaxIterations, *arg.limit, arg.imgFile, *arg.Width, *arg.Height)
}

func main() {
	var arguments = Init()
	if !*arguments.ui {
		fmt.Println(arguments)
		generateMandel(arguments)
	} else {
		// generate standard image
		arguments.imgFile = "public/mandel.png"
		generateMandel(arguments)
		
		app := neo.App()
		app.Templates("./templates/*")
		app.Serve("/public", "./public")
		app.Get("/", func(ctx *neo.Ctx) (int, error) {
			return 200, ctx.Res.Tpl("index", arguments)
		})
		app.Post("/mandel", func(ctx *neo.Ctx) (int, error) {
			ctx.Req.ParseForm()
			*arguments.X1 = parseFormFloat64("x1", ctx)
			*arguments.X2 = parseFormFloat64("x2", ctx)
			*arguments.Y1 = parseFormFloat64("y1", ctx)
			*arguments.Y2 = parseFormFloat64("y2", ctx)
			*arguments.MaxIterations = parseFormUint("itt", ctx)
			*arguments.Width = parseFormUint("width", ctx)
			*arguments.Height = parseFormUint("height", ctx)
			arguments.imgFile = "public/mandel.png"
			generateMandel(arguments)
			ctx.Res.Header().Set("Location", "/")
			return 303, nil
		})
		app.Start()
	}

}

func parseFormFloat64(str string, ctx *neo.Ctx) float64 {
	res, _ := strconv.ParseFloat(ctx.Req.Form[str][0], 64)
	return res
}
func parseFormUint(str string, ctx *neo.Ctx) uint {
	res, _ := strconv.ParseUint(ctx.Req.Form[str][0],10, 64)
	return uint(res)
}

func generateMandel(arguments Arguments) {
	mandel := model.EscapeTime(*arguments.X1, *arguments.X2, *arguments.Y1, *arguments.Y2, *arguments.limit,
		*arguments.MaxIterations, *arguments.Width, *arguments.Height)
	output.ImagePNG(mandel, arguments.imgFile, *arguments.Width, *arguments.Height, *arguments.MaxIterations)
}

// Init retrieves flags from the commandline
// every flag sets a reasonable default for mandelbrot sets
func Init() Arguments {

	args := Arguments{
		flag.Float64("x1", -2.5, "start of the x-axis"),
		flag.Float64("x2", 1.0, "end of the x-axis"),
		flag.Float64("y1", -1.5, "start of the y-axis"),
		flag.Float64("y2", 1.5, "end of the y-axis"),
		flag.Float64("limit", 4, "limit for divergence"),
		flag.Uint("iterations", 1000, "maximum iterations for every pixel"),
		flag.Uint("width", 800, "width of the picture in pixels"),
		flag.Uint("height", 600, "height of the picture in pixels"),
		flag.Bool("ui", false, "start web ui"),
		"mandel.png",
	}
	flag.Usage = showUsage
	flag.Parse()

	//remaining commandline-args
	fileName := flag.Args()
	if len(fileName) == 1 {
		args.imgFile = fileName[0]
	}

	return args
}

func showUsage() {
	fmt.Println(`Usage: mandel [OPTIONS] [filename]
	
Without options and arguments mandel generates a Mandelbrot fractal with default values as "mandel.png" in the current directory.

Coordinates can be set as Float64 for the X axis and the Y axis. The first coordinate should be the smaller one.
	-x1		defaults to -2.5
	-x2		defaults to  1.0
	-y1		defaults to -1.5
	-y2 		defaults to  1.5
	
Calculation flags
	-iterations	maximum iterations for every pixel, defaults to 1000
	-limit		ends the calculation if the z values exceed this limit, defaults to 4
	
Picture size can be set as Uint
	-width		defaults to 800
	-height		defaults to 600
	
mandel has a web ui built in. It can be startet with flag -ui. All other flags and arguments are ignored when started this way.`)
}
