package main

import (
	"os"
	"testing"
)

func TestInit(t *testing.T) {
	// restore args afterwards
	oldArgs := os.Args
	defer func() { os.Args = oldArgs }()
	
	os.Args = []string{"mandel", "-x2=-3.0", "test.jpg"}  
	wantX1 := -2.5
	wantX2 := -3.0
	wantImgFile := "test.jpg"
	got := Init()
	if *got.X1 != wantX1 {
		t.Errorf("arguments.x1: got %v want %v", *got.X1, wantX1)
	}
	if *got.X2 != wantX2 {
		t.Errorf("arguments.x2: got %v want %v", *got.X2, wantX2)
	}
	if got.imgFile != wantImgFile {
		t.Errorf("arguments.ImgFile: got %v want %v", got.imgFile, wantImgFile)
	}
}