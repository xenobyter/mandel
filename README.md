# Mandelbrot generator
mandel is a generator for the well-known [Mandelbrot fractal](https://en.wikipedia.org/wiki/Mandelbrot_set) written in go.

## Build
`go build mandel.go`

## Usage
`mandel [OPTIONS] [filename]`

Without options and arguments mandel generates a Mandelbrot fractal with default values as "mandel.png" in the current directory.

## Coordinates
Coordinates can be set as Float64 for the X axis and the Y axis. The first coordinate should be the smaller one.

|Parameter|Default|
|-|-:|
|-x1|-2.5|
|-x2| 1.0|
|-y1|-1.5|
|-y2| 1.5|

## Calculation flags

|Parameter|Description|Default|
|-|-|-:|
|-iterations|maximum iterations for every pixel|1000
|-limit|ends the calculation if the z values exceed this limit|4

## Configure the picture
The filename can be set as argument. The current version of mandel generates png-images only.

The size of the generated picture can be set as Uint.

|Parameter|Default|
|-|-:|
|-width|800|
|-height|600|

## Web ui
Mandel has a web ui built in. It can be startet with flag `-ui`. All other flags and arguments are ignored when started this way. Navigate your Browser to `http://localhost:3000`. 